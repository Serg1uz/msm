import sys
from time import sleep
import requests

import undetected_chromedriver as uc

driver = uc.Chrome()
driver.get('https://www.mscdirect.com/LogonForm')
sleep(5)
driver.find_element_by_css_selector('#username').send_keys('MDLLC')
driver.find_element_by_css_selector('#password').send_keys('AhMnGDa7Q0GF')
driver.find_element_by_css_selector('#userLoginButton').click()

agent = driver.execute_script("return navigator.userAgent")



host = 'www.mscdirect.com'
headers = {
    'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.82 Safari/537.36',
    'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
    'Accept-Encoding': 'gzip, deflate, br',
    'Accept-Language': 'en,en-US;q=0.9,ru;q=0.8',
    'Cache-Control': 'no-cache',
    'Connection': 'keep-alive',
    'Host': f'{host}',
    'Pragma': 'no-cache',
    'sec-ch-ua': '"Google Chrome";v="89", "Chromium";v="89", ";Not A Brand";v="99"',
    'sec-ch-ua-mobile': '?0',
    'Sec-Fetch-Dest': 'document',
    'Sec-Fetch-Mode': 'navigate',
    'Sec-Fetch-Site': 'same-origin',
    'Sec-Fetch-User': '?1',
    'Upgrade-Insecure-Requests': '1'
}


session = requests.session()
session.headers = headers
cookies = driver.get_cookies()
for cookie in cookies:
    session.cookies.set(cookie['name'], cookie['value'])


r = session.get('https://www.mscdirect.com/TypeAheadResultsView?searchterm=65-835-1&_=1592904609998')
json = r.json()
print(json)
#
# new_url = json['MSC_PART_NUMBER']['suggestions'][2]['url']
# r2 = session.get(f'https://{host}{new_url}', verify=False, headers=headers)
