from requests_html import HTMLSession


def save_to_file(text,  name):
    file = open(f'data/{name}l', 'w')
    file.write(text)
    file.close()


def main():
    session = HTMLSession()
    host = 'https://www.mscdirect.com'
    headers = {
        'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.82 Safari/537.36',
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
        'Accept-Encoding': 'gzip, deflate, br',
        'Accept-Language': 'en,en-US;q=0.9,ru;q=0.8',
        'Cache-Control': 'no-cache',
        'Connection': 'keep-alive',
        'Host': f'{host}',
        'Pragma': 'no-cache',
        'sec-ch-ua': '"Google Chrome";v="89", "Chromium";v="89", ";Not A Brand";v="99"',
        'sec-ch-ua-mobile': '?0',
        'Sec-Fetch-Dest': 'document',
        'Sec-Fetch-Mode': 'navigate',
        'Sec-Fetch-Site': 'same-origin',
        'Sec-Fetch-User': '?1',
        'Upgrade-Insecure-Requests': '1'
    }

    data = session.get(host)

    if data.status_code == 200:
        save_to_file(data.text, 'main')
        url = host + '/ProductsHomeView'
        data = session.get(url)

        if data.status_code == 200:
            save_to_file(data.text, 'all_products')
            url = host + '/browse/Abrasives?navid=12100008'
            data = session.get(url)
            save_to_file(data.text, 'abrasives')


if __name__ == '__main__':
    main()
