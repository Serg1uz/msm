from collections import namedtuple

# from selenium import webdriver
# from selenium.webdriver.common.by import By
# from selenium.webdriver.support.ui import WebDriverWait
# from selenium.webdriver.support import expected_conditions as EC
import undetected_chromedriver as uc

Categories = namedtuple("Categories", "id name link parent islast")


class Parser:

    def __init__(self):
        self.host = 'www.mscdirect.com'
        self.main_url = f'https://{self.host}'

        # options = webdriver.ChromeOptions()
        # # options.add_argument('--disable-infobars')
        # options.add_argument('disable-infobars')
        # options.add_argument('--disable-extensions')
        # options.add_argument('--profile-directory=Default')
        # # options.add_argument('--incognito')
        # options.add_argument('--disable-plugins-discovery')
        # options.add_argument('--start-maximized')
        # options.add_argument(
        #     f'user-agent=Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.79 Safari/537.36')
        # # self.driver = webdriver.Chrome(chrome_options=options)
        # self.driver = webdriver.Firefox()
        self.driver = uc.Chrome()
        self.driver.get(self.main_url)
        self.driver.implicitly_wait(25)
        self.update_cookie()

        self.all_categories = []

    def start_parsing(self):
        self.get_main_categories()
        print(self.all_categories)

        for current_categorie in self.all_categories:
            self.get_sub_categories(current_categorie)

        print(self.all_categories)

    def get_main_categories(self):
        url = self.main_url + '/ProductsHomeView'
        parent = 0
        self.driver.get(url)
        self.driver.implicitly_wait(5)
        block_categories = self.driver.find_elements_by_id('v4-browse-nav-list')
        if len(block_categories) > 0:
            index = 1
            list_categories = block_categories[0].find_elements_by_tag_name('a')
            if len(list_categories) > 0:
                for categoriya in list_categories:
                    current_cat = Categories(index, categoriya.text, categoriya.get_attribute('href'), parent, False)
                    self.all_categories.append(current_cat)
                    index += 1


    def get_products(self, categories: Categories = None):
        print(f'Get Products - {categories}')




    def get_categories(self, block_categories):
        pass

    def die(self):
        self.driver.quit()

    def update_cookie(self):
        pass
        # cookies = self.driver.get_cookies()
        # if len(cookies) > 0:
        #     self.driver.delete_all_cookies()
        #     for cookie in cookies:
        #         self.driver.add_cookie(cookie)

    def get_sub_categories(self, current_categorie: Categories):
        print(current_categorie)
        url = current_categorie.link
        parent = current_categorie.id
        self.driver.get(url)
        self.driver.implicitly_wait(5)

        block_refinment = self.driver.find_elements_by_id('popluateAjaxResponse')
        if len(block_refinment) > 0:
           self.get_products(current_categorie)

        block_categories = self.driver.find_elements_by_id('v4-browse-nav-list')
        if len(block_categories) > 0:
            sub_categories = []
            index = 1
            list_categories = block_categories[1].find_elements_by_tag_name('a')
            if len(list_categories) > 0:
                for categoriya in list_categories:
                    current_cat = Categories(index, categoriya.text, categoriya.get_attribute('href'), parent, False)
                    sub_categories.append(current_cat)
                    index += 1

            self.all_categories += sub_categories

            for sc in sub_categories:
                self.get_sub_categories(sc)



def main():
    parser = Parser()
    parser.start_parsing()
    # parser.die()


if __name__ == '__main__':
    main()
