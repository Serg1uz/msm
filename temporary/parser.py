import time

import requests
from bs4 import BeautifulSoup as BS
from collections import namedtuple
import os.path
from os import path


def check_file(file_name: str) -> bool:
    return path.exists(f'data/{file_name}.html')


class Parser:
    Categories = namedtuple("Categories", "id name link parent")

    def __init__(self):
        self.host = 'www.mscdirect.com'
        self.headers = {
            'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.82 Safari/537.36',
            'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
            'Accept-Encoding': 'gzip, deflate, br',
            'Accept-Language': 'en,en-US;q=0.9,ru;q=0.8',
            'Cache-Control': 'no-cache',
            'Connection': 'keep-alive',
            'Host': f'{self.host}',
            'Pragma': 'no-cache',
            'sec-ch-ua': '"Google Chrome";v="89", "Chromium";v="89", ";Not A Brand";v="99"',
            'sec-ch-ua-mobile': '?0',
            'Sec-Fetch-Dest': 'document',
            'Sec-Fetch-Mode': 'navigate',
            'Sec-Fetch-Site': 'same-origin',
            'Sec-Fetch-User': '?1',
            'Upgrade-Insecure-Requests': '1'
        }
        self.session = requests.session()
        self.main_url = f'https://{self.host}'

        main_page = self.session.get(self.main_url, verify=False, headers=self.headers)
        cookies = main_page.cookies.get_dict()

        for k,v in cookies:
            print(f'{k} = {v}')



        self.session.cookies.update(main_page.cookies)

        self.categories_list = []

    def download_page(self, name: str, url: str):
        try:
            data = self.session.get(url, verify=False, headers=self.headers)
            self.session.cookies.update(data.cookies)
        except Exception as e:
            return False, f"Can't receive Categories Page (v1) + {e}"
        if data.status_code != 200:
            return False, "Can't receive Categories Page"

        file = open(f'data/{name}.html', 'w')
        file.write(data.text)
        file.close()

    def get_categories(self, categories: Categories = None):
        if categories is None:
            url = self.main_url + '/ProductsHomeView'
            parent = 0
            file_name = 'main'
        else:
            url = self.main_url + categories.link
            parent = categories.id
            file_name = categories.name

        if not check_file(file_name):  self.download_page(file_name, url)

        file = open(f'data/{file_name}.html', 'r')
        data = file.read()
        file.close()

        soup = BS(data, 'html.parser')

        block_refinment = soup.find("div", {"id": "popluateAjaxResponse"})
        if block_refinment:
            print(block_refinment)
            return True, None

        block_categories = soup.find("div", {"id": "v4-browse-nav-list"})

        list_categories = block_categories.find_all('a', href=True)
        result = []
        index = 1
        for categorie in list_categories:
            categ = self.Categories(index, categorie.text, categorie['href'], parent)
            result.append(categ)
            index += 1
        return True, result

    def get_products(self, categories: Categories = None):
        code, new_categories = self.get_categories(categories)
        if not code:
            return False, 'Somthing wrong'

        if new_categories is not None:
            self.categories_list += new_categories
            for current_categories in new_categories:
                code, products = self.get_products(current_categories)

        # file_name = categories.name
        # file = open(f'data/{file_name}.html', 'r')
        # data = file.read()
        # file.close()
        #
        # soup = BS(data, 'html.parser')
        #
        # block_refinment = soup.find("div", {"id": "popluateAjaxResponse"})
        products = None

        return True, products

    def search_products(self, part_number: str, index: int):
        url = f'https://www.mscdirect.com/browse/?searchterm={part_number}&hdrsrh=true&rd=k'
        time.sleep(300)
        response = self.session.get(url, verify=False, headers=self.headers)

        file = open(f'data/{index}-pn-{part_number}.html', 'w')
        file.write(response.text)
        file.close()


def main():
    parser = Parser()
    for i in range(0, 3):
        parser.search_products('SPI 13-156-5', i)
    #
    # code, products = parser.get_products()
    # print(products)


if __name__ == "__main__":
    main()
