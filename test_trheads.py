import undetected_chromedriver as uc
from threading import Thread


class Parser:

    def __init__(self):
        self.driver = uc.Chrome()


    def open_url(self):
        self.driver.get('https://google.com')

    def search(self, text):
        search = self.driver.find_element_by_name('q')
        search.send_keys(text)
        search.submit()

    def get_result(self, text):
        self.open_url()
        self.search(text)
        result = self.driver.find_elements_by_id('result-stats')[0].text
        self.driver.quit()
        return result


class ThreadParser(Thread):
    def __init__(self, search_text):
        super(ThreadParser, self).__init__()
        parser = Parser()
        self.result = parser.get_result(search_text)


if __name__ == '__main__':
    threads = []
    for i in range(5):
        thread = ThreadParser(f'test data {i}')
        thread.start()
        threads.append(thread)

    output_data = []
    for t in threads:
        output_data += t.result

    print(output_data)
