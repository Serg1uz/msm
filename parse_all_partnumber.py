import csv
import getopt
import logging
import math
import os
import re
import sys
import threading
from collections import namedtuple
from random import randint
from time import sleep
from typing import List, Tuple

import requests
import undetected_chromedriver as uc
from bs4 import BeautifulSoup as BS
from tqdm import tqdm
import warnings

COUNT_THREADING = 1

warnings.filterwarnings("ignore")

logging.basicConfig(
    filename='parse_all.log',  # saving log to filename
    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', level=logging.INFO  # DEBUG
)

logging.info('logger started')
logger = logging.getLogger(__name__)

Input_Data = namedtuple("InputData", "part_number")
Output_Data = namedtuple("OutputData", "brand part_number s_part_number msc_part_number stock price "
                                       "web_price total_price qty name product_details specs images "
                                       "categories link authorized_price")
Logged_Data = namedtuple("LoggedData", "part_number json")

# статичное указания входящего csv файла
INPUT_FILE = 'sp1675.csv'


# выбрать необходимого пользователя путем закомментирования и расскоментирования значения USERNAME и PASSWORD
USERNAME = 'MDLLC'
PASSWORD = 'AhMnGDa7Q0GF'
# USERNAME = 'marketing@primebuy.com'
# PASSWORD = 'M@KDQhNrE1xuxdfriv'

HOST = 'www.mscdirect.com'
HEADERS = {
    'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.82 Safari/537.36',
    'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
    'Accept-Encoding': 'gzip, deflate, br',
    'Accept-Language': 'en,en-US;q=0.9,ru;q=0.8',
    'Cache-Control': 'no-cache',
    'Connection': 'keep-alive',
    'Host': f'{HOST}',
    'Pragma': 'no-cache',
    'sec-ch-ua': '"Google Chrome";v="89", "Chromium";v="89", ";Not A Brand";v="99"',
    'sec-ch-ua-mobile': '?0',
    'Sec-Fetch-Dest': 'document',
    'Sec-Fetch-Mode': 'navigate',
    'Sec-Fetch-Site': 'same-origin',
    'Sec-Fetch-User': '?1',
    'Upgrade-Insecure-Requests': '1'
}

limit = 0


def check_header(headers: List) -> bool:
    """
    Checker header csv-file equals type Input_Data headers
    :param headers: list
    :return: bool
    """
    input_data_headers = [name for name in Input_Data._fields]
    return headers == input_data_headers


def read_csv(file_name: str = None) -> Tuple[bool, List[Input_Data]]:
    """
    Reading input csv-file for data processing
    :param file_name: str filename if None filename default equals input.csv
    :return: tuple[bool, list[input_data]] when error return [False, empty lit]
    else [True, list[input_data]
    """
    result = []
    file_src = file_name if file_name is not None else INPUT_FILE
    try:
        with open(file_src) as infile:
            reader = csv.DictReader(infile)
            list_fields = [name for name in reader.fieldnames]
            if check_header(list_fields):
                # next(reader, None)
                for row in reader:
                    data = Input_Data(row['part_number'])
                    result.append(data)
            else:
                logger.error('bad read file')
                return False, []
    except Exception as e:
        logger.error(f'Error!!! error when read input data - {e}')
        return False, []
    return True, result


def get_data_logon():
    driver = uc.Chrome()
    driver.get('https://www.mscdirect.com/LogonForm')
    sleep(6)
    try:
        driver.find_element_by_css_selector('#username').send_keys(USERNAME)
        driver.find_element_by_css_selector('#password').send_keys(PASSWORD)
        driver.find_element_by_css_selector('#RememberUsername').click()
        driver.find_element_by_css_selector('#userLoginButton').click()
    except Exception as e:
        logger.debug(msg='Can not login')

    agent = driver.execute_script("return navigator.userAgent")
    cookies = driver.get_cookies()
    sleep(2)
    result = len(driver.find_elements_by_css_selector('#main-account-logout')) > 0
    # driver.quit()
    return result, agent, cookies, driver


def start_session(agent, cookies):
    headers = HEADERS
    session = requests.session()
    for cookie in cookies:
        session.cookies.set(cookie['name'], cookie['value'])
    session.headers = HEADERS

    return session, headers


def get_first_response(input_part_number, session, headers, cookies):
    try:
        url = f'https://www.mscdirect.com/TypeAheadResultsView?searchterm={input_part_number}&_=1592904609998'
        r = session.get(url)
        return r.status_code, r.json()
    except Exception as e:
        logger.debug(f'Cant get first pasge - {url}')
        return 400, None


def get_hide_info(session, headers, msc_part):
    r = session.get(f'https://content.syndigo.com/page/19058212-07fb-4f76-b772-94600ba408fb/{msc_part}.json',
                    headers=headers, verify=False)
    if r.status_code != 200 or len(r.json()) <= 0:
        logger.debug(f'not found hiden product details for msc_part {msc_part}')
        return ''

    widgets = r.json()['experiences']['experiences']['widgets']
    result = ''
    for widget in widgets:
        if widget['headerText'] == 'Documents':
            continue
        result += f'<h3>{widget["headerText"]}</h3> \n\r {widget["items"]["description"]} \n\r'
    return result


def download_images(src, brand, msc_part):
    index = 0
    for source in src:
        try:
            brand = f'{brand}'
            parent = 'data'
            path = os.path.join(parent, brand)
            if not os.path.exists(path):
                os.mkdir(path)
            part = msc_part.replace(' ', '')
            if index > 0:
                part += f'_{index}'

            iteration = 0
            while iteration <= 3:
                try:
                    response = requests.get(source)
                    file = open(f'data/{brand}/{part}.jpg', 'wb')
                    file.write(response.content)
                    file.close()
                    break
                except Exception as e:
                    logger.error(e)
                    iteration += 1
            index += 1
        except Exception as e:
            # print(f'When download image something wrong \n\r {e}')
            logger.error(e)
            continue


def get_images(session, headers, block_image, brand, msc_part):
    try:
        images_tag = block_image.find_all('img')
        src = []
        for imag in images_tag:
            src.append(imag.get('src'))

        src = list(set(src))
        src = [f'https://cdn.mscdirect.com/global/images/ProductImages/{source.split("/")[-1].split(".")[0]}.jpg' for
               source in src]
        download_images(src, brand, msc_part)
        # src = [source.split("/")[-1] for source in src]
        index = 0
        src_2 = []
        for source in src:
            part = msc_part if index == 0 else f'{msc_part}_{index}'
            part = f'{part}.jpg'
            src_2.append(part)
            logger.debug(f'Source image {source} rename to {part}')
        return " || ".join(src_2)
    except Exception as e:
        logger.debug(f'Can not get images {brand} - {msc_part}')
        return 'Error!!! '


def get_page(url, brand, s_part_number, driver, session, headers, iteration):
    global limit
    sleep(0.5)
    limit += 1
    if limit < 4:
        r = session.get(url, headers=HEADERS, verify=False)
        soup = BS(r.text, 'html.parser')
    else:
        driver.get(url)
        cookies = driver.get_cookies()
        for cookie in cookies:
            session.cookies.set(cookie['name'], cookie['value'])
        soup = BS(driver.page_source, 'html.parser')
        limit = 0

    if soup.find('div', {'id': 'distilIdentificationBlock'}) is not None:
        logger.error(f'STOP PARSING {brand}-{s_part_number} site is block \n\r url: {url}')
        return session, None

    return session, soup


def get_data(url, brand, msc_part, session, headers, authorized, s_part_number, driver):
    # return session, None
    try:

        url = f'https://www.mscdirect.com{url}'
        session, soup = get_page(url, brand, s_part_number, driver, session, headers, 0)
        if soup is None:
            logger.error(f"Skip product - {s_part_number}")
            return session, None

        part_number = get_partnumber(soup)
        part_number = part_number.split(':')[-1].strip()
        if part_number == '':
            part_number = s_part_number

        stock = get_stock(soup)

        price = get_price(soup)
        web_price = get_webprice(soup)
        qty = get_qty(soup)

        total_price = get_totalprice(qty, web_price)

        name = get_name(soup)

        product_details = get_product_details(headers, msc_part, session, soup)

        specs = get_specs(soup)

        categories = get_categories(soup)

        block_image = soup.find("div", {"id": "pdp-prod-image-container"})
        images = get_images(session, headers, block_image, brand, s_part_number)
    except Exception as e:
        logger.error(f'STOP PARSING - {e} \n\r Page: {url}')
        return session, None

    return session, Output_Data(
        brand=brand,
        part_number=part_number,
        s_part_number=s_part_number,
        msc_part_number=msc_part,
        stock=stock,
        price=price,
        web_price=web_price,
        total_price=total_price,
        qty=qty,
        name=name,
        product_details=product_details,
        specs=specs,
        images=images,
        categories=categories,
        link=url,
        authorized_price=authorized
    )


def get_categories(soup):
    try:
        categories = soup.find("div", {"id": "pdp-breadcrumb"}).text
        categories = "/".join(categories.split('/')[1:-1]).strip()
    except Exception as e:
        logger.debug('cant get categories ')
        categories = ''
    return categories


def get_specs(soup):
    try:
        details_info1 = soup.find("div", {"id": "pdp-specs"})
        html1 = "".join([str(x) for x in details_info1.contents])
        clear_class = re.compile('class=".*?"')
        html1 = re.sub(clear_class, '', html1)
        specs = html1
    except Exception as e:
        logger.debug('cant get specs ')
        specs = ''
    return specs


def get_product_details(headers, msc_part, session, soup):
    try:
        details_info1 = soup.find("div", {"id": "pdp-features"})
        html1 = "".join([str(x) for x in details_info1.contents]) if details_info1 is not None else ''
        clear_class = re.compile('class=".*?"')
        html1 = re.sub(clear_class, '', html1)
        hiden_block = soup.find_all('div', {"class": "syndi_powerpage.syndigo-shadowed-powerpage"})
        if len(hiden_block) > 0:
            details_info2 = get_hide_info(session, headers, msc_part)
        else:
            details_info2 = ''
        product_details = html1 + details_info2
    except Exception as e:
        logger.debug('cant get product details ')
        product_details = ''
    return product_details


def get_name(soup):
    try:
        name = soup.find("span", {"class": "productTitle"}).text.strip()
    except Exception as e:
        logger.debug('cant get name ')
        name = ''
    return name


def get_totalprice(qty, web_price):
    try:
        total_price = int(qty) \
                      * float(web_price)
    except Exception as e:
        logger.debug('cant get total price ')
        total_price = ''
    return total_price


def get_qty(soup):
    try:
        qty = soup.find("input", {"name": "pdp-quantity"}).get('value')
    except Exception as e:
        logger.debug('cant get qty ')
        qty = ''
    return qty


def get_webprice(soup):
    try:
        web_price = soup.find("span", {"id": "webPriceDiv"}).text.split(' ')[0]
    except Exception as e:
        logger.debug('cant get web_price ')
        web_price = ''
    return web_price


def get_price(soup):
    try:
        price = soup.find("span", {"id": "listPriceDiv"}).text
    except Exception as e:
        logger.debug('cant getprice ')
        price = ''
    return price


def get_stock(soup):
    try:
        stock = soup.find("div", {"id": "v4-pdp-top-notes"}).find("div", {"class": "item"}).find("div", {
            "class": "content"}).text.strip()
    except Exception as e:
        logger.debug('cant get stock data')
        stock = ''
    return stock


def get_partnumber(soup):
    try:
        part_number = soup.find_all("div", {"class": "pdp-part-num"})[1].text
    except Exception as e:
        logger.debug('cant find part number')
        part_number = ''
    return part_number


def save_data(output_file, output_data):
    if os.path.isfile(output_file):
        with open(output_file, 'a') as f:
            w = csv.writer(f)
            w.writerow(output_data)
    else:
        with open(output_file, 'w') as f:
            w = csv.writer(f)
            headers = [name for name in output_data._fields]
            w.writerow(headers)
            w.writerow(output_data)


def parsing(input_data, session, headers, output_file, authorized, cookies, driver):
    with tqdm(total=len(input_data), file=sys.stdout) as pbar:
        for row in input_data:
            pbar.update(1)
            try:
                code, json = get_first_response(row.part_number, session, headers, cookies)
                log_data = Logged_Data(row.part_number, json)
                save_data('log_data.csv',log_data)
                if code != 200 or len(json) <= 0:
                    logger.debug(f'Cant get first data for search part number {row.part_number}')
                    continue
                suggestions_data = json['MSC_PART_NUMBER']['suggestions']
                for sd in suggestions_data:
                    try:
                        url = sd['url']
                        brand = sd['brand']
                        msc_part = sd['partNumber']
                        part_number = row.part_number
                        sleep(randint(1, 2))
                        session, output_data = get_data(url, brand, msc_part, session, headers, authorized, part_number,
                                                        driver)
                        if output_data is None:
                            logger.error(f'Cant get data (skip) - {brand} {part_number}')

                            skip_part = Input_Data(part_number=part_number)
                            save_data(f'skiped.csv', skip_part)
                            # pbar.update(1)
                            continue
                        save_data(output_file, output_data)
                    except Exception as e:
                        logger.error(f'OOOOPS222 - {e}')
                        continue
            except Exception as e:
                logger.error(f'OOOOPS - {e} \n\r {row}')
                continue
                # pbar.update(1)



def split_list(lst: List, count_of_part: int):
    return [lst[i:i + count_of_part] for i in range(0, len(lst), count_of_part)]


def main(argv):
    input_file = INPUT_FILE
    output_file = ''
    try:
        opts, args = getopt.getopt(argv, "hi:o:", ["ifile=", "ofile="])
    except  getopt.GetoptError:
        logger.error(msg='no need param must be: parse_all_partnumber.py -i <inputfile> -o <outputfile>')
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            print('parse_all_partnumber.py -i <inputfile> -o <outputfile>')
            sys.exit()
        elif opt in ("-i", "--ifile"):
            input_file = arg
        elif opt in ("-o", "--ofile"):
            output_file = arg
    if output_file == '':
        output_file = f'out_{input_file}'

    result, input_data = read_csv(input_file)
    if not result:
        logger.debug(f'Somthing wrong when read data from {input_file}')
        sys.exit(2)

    result, agent, cookies, driver = get_data_logon()
    if not result:
        logger.info(f'Can not login to site by {USERNAME}')

    session, headers = start_session(agent, cookies)

    parsing(input_data, session, headers, output_file, result, cookies, driver)

    # count_threading = COUNT_THREADING
    # count_of_part = math.ceil(len(input_data) / count_threading)
    # splitting_list = split_list(input_data, count_of_part)
    #
    # # with tqdm(total=count_threading, file=sys.stdout) as pbar_th:
    # min = count_threading if count_threading <= len(splitting_list) else len(splitting_list)
    # for i in range(min):
    #     of = f'part{i}_{output_file}'
    #     thread = threading.Thread(target=parsing,
    #                               args=(splitting_list[i], session, headers, of, result, cookies, driver))
    #     thread.start()
    #     # pbar_th.update(1)
    # # parsing(input_data, session, headers, output_file, result, cookies)


if __name__ == '__main__':
    main(sys.argv[1:])
