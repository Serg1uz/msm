import csv
import os
from collections import namedtuple
from threading import Thread
import sys
import re
from tqdm import tqdm
import logging
import requests
import undetected_chromedriver as uc

from typing import List, Tuple

logging.basicConfig(
    filename='my_runtime_log.log',  # saving log to filename
    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', level=logging.INFO  # DEBUG
)
logging.info('logger started')
logger = logging.getLogger(__name__)

Input_Data = namedtuple("InputData", "brand part_number msc_part_number")
Output_Data = namedtuple("OutputData", "brand part_number msc_part_number stock price "
                                       "web_price total_price qty name product_details specs images "
                                       "categories link authorized_price")
INPUT_FILE = 'spi4.csv'
OUTPUT_FILE = f'out_{INPUT_FILE}'


# https://www.mscdirect.com/product/details/54325766?rItem=54325766

# INPUT_FILE = 'input.csv'

def check_header(headers: List) -> bool:
    """
    Checker header csv-file equals type Input_Data headers
    :param headers: list
    :return: bool
    """
    input_data_headers = [name for name in Input_Data._fields]
    return headers == input_data_headers


def download_images(src: List, output_data: Output_Data) -> None:
    index = 0
    for source in src:
        try:

            brand = output_data.brand
            parent = 'data'
            path = os.path.join(parent, brand)
            if not os.path.exists(path):
                os.mkdir(path)
            part = output_data.part_number.replace(' ', '')
            if index > 0:
                part += f'_{index}'
            # path = os.path.join(path, part)
            # if not os.path.exists(path):
            #     os.mkdir(path)

            iteration = 0
            while iteration <= 3:
                try:
                    response = requests.get(source)
                    file = open(f'data/{brand}/{part}.jpg', 'wb')
                    file.write(response.content)
                    file.close()
                    break
                except Exception as e:
                    logger.error(e)
                    iteration += 1
            index += 1
        except Exception as e:
            # print(f'When download image something wrong \n\r {e}')
            logger.error(e)
            continue


def parsing(input_data: List[Input_Data], driver) -> Tuple[bool, List[Output_Data]]:
    result = []
    parser = Parser(driver)
    print(f'start parting parsing part number - {len(input_data)}')
    with tqdm(total=len(input_data), file=sys.stdout) as pbar:
        for data in input_data:
            try:
                output_data = parser.parse_part_number(data)
                result.append(output_data)
            except Exception as e:
                logger.error(f'OOOPS - {e}')
                continue
            pbar.update(1)

        # print(parser.parse_part_number(data))
    return True, result


class InputData:
    """
    Class for inputs data
    """

    def __init__(self):
        """
        Initialization method create fields:
        code - bool
        data - list[Input_Data]
        error_message - str
        """
        self.code, self.data = self.read_csv()
        self.error_message = ''

    def read_csv(self, file_name: str = None) -> Tuple[bool, List[Input_Data]]:
        """
        Reading input csv-file for data processing
        :param file_name: str filename if None filename default equals input.csv
        :return: tuple[bool, list[input_data]] when error return [False, empty lit]
        else [True, list[input_data]
        """
        result = []
        file_src = file_name if file_name is not None else INPUT_FILE
        try:
            with open(file_src) as infile:
                reader = csv.DictReader(infile)
                list_fields = [name for name in reader.fieldnames]
                if check_header(list_fields):
                    # next(reader, None)
                    for row in reader:
                        data = Input_Data(row['brand'], row['part_number'], row['msc_part_number'])
                        result.append(data)
                else:
                    self.error_message = 'Headers into csv file not equals in need data'
                    logger.error('bad read file')
                    return False, []
        except Exception as e:
            print(f'Error!!! error when read input data - {e}')
            self.error_message = e
            return False, []
        return True, result


class Parser:

    def __init__(self, driver):
        self.host = 'www.mscdirect.com'
        self.main_url = f'https://{self.host}'
        self.driver = driver
        self.driver.get(self.main_url)
        self.login_in = False
        self.sign_in()

    def parse_part_number(self, input_data: Input_Data, by_msc: bool = False) -> Output_Data:
        logger.debug(f'Search product {input_data.brand} - {input_data.part_number}')
        url = f'{self.main_url}/browse/lookahead/' \
              f'?searchterm={input_data.brand} ({input_data.part_number})' \
              f'&hdrsrh=true '

        output_data = Output_Data(input_data.brand, input_data.part_number, '',
                                  '', '', '', '', '', '', '', '', '', '', url, self.login_in)

        self.driver.get(url)

        if self.is_page_product():
            output_data = self.parse_product(output_data)
        elif self.is_search_product_page():
            output_data = self.search_product(output_data)

        return output_data

    def parse_product(self, output_data):
        price, web_price, total_price = self.get_prices()

        result = Output_Data(
            brand=output_data.brand,
            part_number=self.get_part_number() if self.get_part_number().strip() != '' else output_data.part_number,
            msc_part_number=self.get_msc_part_number(),
            stock=self.get_stock(),
            price=price,
            web_price=web_price,
            total_price=total_price,
            qty=self.get_qty(),
            product_details=self.get_product_details(),
            name=self.get_product_name(),
            specs=self.get_specs(),
            categories=self.get_categories(),
            images=self.get_images(output_data),
            link=self.driver.current_url,
            authorized_price=self.login_in
        )

        return result

    def get_categories(self):
        categories = self.driver.find_elements_by_id('pdp-breadcrumb')[0].text
        categories = "/".join(categories.split('/')[1:-1]).strip()
        return categories

    def get_specs(self):
        specs_block = self.driver.find_elements_by_id('pdp-featuresspecs')[0].find_elements_by_id('pdp-specs')

        if len(specs_block) <= 0:
            return ''

        source = specs_block[0].get_attribute('innerHTML')
        clear_class = re.compile('class=".*?"')
        result = re.sub(clear_class, '', source)
        return result

    def get_product_details(self):
        result = ''
        data = self.driver.find_elements_by_class_name('syndi_powerpage.syndigo-shadowed-powerpage')
        if len(data) > 0:
            self.driver.execute_script("toggleTabs();")
            self.driver.execute_script("arguments[0].scrollIntoView();", data[0])
            sd = self.driver.find_elements_by_class_name('syndi_powerpage.syndigo-shadowed-powerpage')
            el = self.driver.execute_script("return arguments[0].shadowRoot", sd[0])
            blocks = el.find_elements_by_class_name('syndigo-powerpage-grid-widget')
            result = ''
            for block in blocks:
                header = block.find_elements_by_class_name('syndigo-widget-section-header')
                if len(header) <= 0:
                    continue
                if header[0].text == 'Documents':
                    continue
                res = header[0].get_attribute('outerHTML')
                result += res
                datas = block.find_elements_by_class_name('syndigo-featureset-feature-nooverlay-inner')
                if len(data) > 0:
                    res = datas[0].get_attribute('innerHTML')
                    result += res

        data = self.driver.find_elements_by_id('pdp-features')
        if len(data)>0:
            result += data[0].get_attribute('innerHTML')

        clear_class = re.compile('class=".*?"')
        result = re.sub(clear_class, '', result)
        return result

    def get_qty(self):
        return self.driver.find_element_by_name('pdp-quantity').get_attribute('value')

    def get_prices(self):
        web_price = ''
        if len(self.driver.find_elements_by_class_name('pdp-list-price')) > 0:
            price = self.driver.find_elements_by_class_name('pdp-list-price')[1].text.split(" ")[0][1:]
            web_price = self.driver.find_elements_by_class_name('atc-pdpPrice')[0].text.split(" ")[0][1:]
        else:
            price = self.driver.find_elements_by_class_name('atc-pdpPrice')[0].text.split(" ")[0][1:]
        total_price = self.driver.find_elements_by_id('pdp-item-price')[0].text.split(" ")[0][1:]
        return price, web_price, total_price

    def get_stock(self):
        return self.driver.find_element_by_id('v4-pdp-top-notes').find_element_by_class_name('item') \
            .find_element_by_class_name('content').text

    def get_msc_part_number(self):
        return self.driver.find_elements_by_class_name('pdp-part-num')[0].text.split(':')[-1].strip()

    def get_part_number(self):
        part_number = self.driver.find_elements_by_class_name('pdp-part-num')[1].text
        part_number = part_number.split(':')[-1].strip()
        return part_number

    def is_page_product(self):
        return len(self.driver.find_elements_by_id('pdp-top-content')) > 0

    def get_images(self, output_data: Output_Data):
        block_images = self.driver.find_elements_by_id('pdp-prod-image-container')[0]
        images_tag = block_images.find_elements_by_tag_name('img')
        src = []
        for imag in images_tag:
            src.append(imag.get_attribute('src'))

        src = list(set(src))
        src = [f'https://cdn.mscdirect.com/global/images/ProductImages/{source.split("/")[-1].split(".")[0]}.jpg' for
               source in src]
        download_images(src, output_data=output_data)
        # src = [source.split("/")[-1] for source in src]
        index = 0
        src_2 = []
        for source in src:
            part = output_data.part_number if index == 0 else f'{output_data.part_number}_{index}'
            part = f'{part}.jpg'
            src_2.append(part)
            logger.debug(f'Source image {source} rename to {part}')
        return " || ".join(src_2)

    def is_search_product_page(self):
        return len(self.driver.find_elements_by_id('popluateAjaxResponse')) > 0

    def search_product(self, output_data):
        search_result = self.driver.find_elements_by_class_name('sub.header')[0]

        search_panel = self.driver.find_elements_by_id('v4-tn-refinements-column')[0]
        s_c = search_panel.find_elements_by_class_name('ui.card')

        need_brand = None
        need_cart = None

        for cart in s_c:
            header = cart.find_element_by_class_name('ui.small.header')
            if header.text.strip() == 'Brand':
                need_cart = cart

        if need_cart is not None:
            list_brand = need_cart.find_elements_by_class_name('ui.list')[0]
            for brand in list_brand:
                brand_name = brand.get_attribute('data_val')
                if brand_name == output_data.brand:
                    need_brand = brand
                    break

        if (search_result.text.split(':')[0] == 'No results for'
                or (len(s_c) <= 0
                    or len(need_cart) <= 0
                    or len(need_brand) <= 0)):
            details = f'Not result for search  link for search {self.driver.current_url} '
        else:
            details = f'Something wrong link for search {self.driver.current_url}'

        result = Output_Data(output_data.brand, output_data.part_number, '', '', '', '', '', '', '', details, '', '',
                             '', self.driver.current_url, self.login_in)
        return result

    def get_product_name(self):
        return self.driver.find_elements_by_class_name('pdp.productTitle')[0].text.strip()

    def sign_in(self):
        """
        Учетка Megadepot:
        User Name - MDLLC
        Password - AhMnGDa7Q0GF
        На МД только один нам бренд нужен: SPI, пример товара:
        https://www.mscdirect.com/product/details/01297068?orderedAs=20-757-1&pxno=4773307&rItem=01297068

        Учетка Prime Buy:
        User Name - marketing@primebuy.com
        Password  - M@KDQhNrE1xuxdfriv
        """
        self.driver.get('https://www.mscdirect.com/LogonForm')
        username_input = self.driver.find_element_by_css_selector('#username')
        username_input.send_keys('MDLLC')

        password_input = self.driver.find_element_by_css_selector('#password')
        password_input.send_keys('AhMnGDa7Q0GF')

        remember_checkbox = self.driver.find_element_by_css_selector('#RememberUsername')
        remember_checkbox.click()

        login_btn = self.driver.find_element_by_css_selector('#userLoginButton')
        login_btn.click()

        logout = self.driver.find_elements_by_css_selector('#main-account-logout')
        self.login_in = len(logout) > 0


class ThreadParser(Thread):
    def __init__(self, input_data: List[Input_Data], driver):
        super(ThreadParser, self).__init__()
        self.code, self.result = parsing(input_data, driver)
        self.error_message = ''


def split_list(lst: List, count_of_part: int) -> List:
    """
    Splitting List for equals part by count
    :param lst: List input data
    :param count_of_part: int count in part
    :return: List
    """
    return [lst[i:i + count_of_part] for i in range(0, len(lst), count_of_part)]


def main():
    input_data = InputData()

    if not input_data.code:
        print(f'Error when read input data \n\r '
              f'With error message {input_data.error_message}')
        exit()

    count_threading = 1
    # count_data = len(input_data.data)
    # # _, output_data = parsing(input_data.data)
    # if count_data in range(10, 21):
    #     count_threading = 2
    # elif count_data in range(21, 100):
    #     count_threading = 3
    # elif count_data > 101:
    #     count_threading = 4

    # count_of_part = count_data // count_threading
    # splitting_list = split_list(input_data.data, count_of_part)
    splitting_list = [input_data.data]
    print(f'start parsing total part - {count_threading}')
    threads = []
    drivers = []
    for i in range(count_threading):
        driver = uc.Chrome()
        drivers.append(driver)
        thread = ThreadParser(splitting_list[i], drivers[i])
        thread.start()
        threads.append(thread)

    output_data = []
    for t in threads:
        output_data += t.result


    print(len(output_data))

    with open(OUTPUT_FILE, 'w') as f:
        w = csv.writer(f)
        headers = [name for name in Output_Data._fields]
        w.writerow(headers)
        w.writerows([(datas.brand, datas.part_number, datas.msc_part_number, datas.stock, datas.price,
                      datas.web_price, datas.total_price, datas.qty, datas.name, datas.product_details,
                      datas.specs, datas.images, datas.categories, datas.link, datas.authorized_price) for datas in
                     output_data])


if __name__ == '__main__':
    main()

# TODO  need check
# Legend Valve	101-004NL
# Legend Valve	101-005NL
# Legend Valve	101-006NL
# Legend Valve	101-045
